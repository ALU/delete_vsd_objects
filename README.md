# Delete VSD Objects script
This script deletes Organizations, its nested entities, and some top-level objects (like Org profiles, etc) saving you *clicks*.

# How to run
1. Install vspk by issuing `pip install vspk`
2. Specify CLI arguments specific to your environment

    **Connection settings** 

    The following argument keys can be used to specify connection settings:

    `-E` Nuage VSD API endpoint. Example: https://10.167.62.11

    `-P` Nuage VSD API endpoint port. Defaults to `8443`, Example: 12433

    `-U` Nuage VSD API user name. Defaults to `csproot`, Example: my_admin

    `-S` Nuage VSD API user password. Defaults to `csproot`, Example: my_pass

    `-O` Nuage VSD API user Organization. Defaults to `csp`, Example: my_org

    **Cherry-picking what to delete**

    `-o <org_name>` Name of the Organization to delete. Defaults to `all` meaning all Orgs will be deleted.

    `-n` Set this flag if you do not want to delete platform settings like Org profiles, NSG Templates, etc.
      
3. Initiate deletion:

```bash
# act as a csproot user. Only API Endpoint should be passed explicitly
python delete_vsd_objects.py -E https://10.167.62.11
```

```bash
# sample output
Deleting entities inside Organization SANTANDER_DRY_RUN
- Deleting entities in L3 Domain: SANTANDER_LAYER3_DOMAIN
-- Deleting entities in Subnet: BRANCH_1
--- Deleting Bridge Interface: NSG1_iface
--- Deleting vPort: NSG1_vport
-- Deleting Subnet name:BRANCH_1 address:192.168.2.0 netmask:255.255.255.0
-- Deleting entities in Subnet: BRANCH_2
--- Deleting Bridge Interface: NSG2_iface
--- Deleting vPort: NSG2_vport
-- Deleting Subnet name:BRANCH_2 address:192.168.3.0 netmask:255.255.255.0
-- Deleting Zone: SANTANDER_SOUTH
-- Deleting entities in Ingress ACL: SANTANDER_POLICY
--- Deleting Rule: CATS_5GQ
-- Deleting Ingress ACL: SANTANDER_POLICY
-- Deleting entities in Ingress ACL: Any
-- Deleting Ingress ACL: Any
-- Deleting entities in Engress ACL: AllowAll
-- Deleting Egress ACL: AllowAll
- Deleting L3 Domain: SANTANDER_LAYER3_DOMAIN
- Deleting entities in L2 Domain: SANTANDER_LAYER2_DOMAIN
- Deleting L2 Domain: SANTANDER_LAYER2_DOMAIN
- Deleting L3 Domain Template: SANTANDER_LAYER3_TEMPLATE
- Deleting L2 Domain Template: SANTANDER_LAYER2_TEMPLATE
- Deleting NSG Instance: NSGV_BRANCH_1
- Deleting NSG Instance: NSGV_BRANCH_2
- Deleting Organization: SANTANDER_DRY_RUN

 ==== Deleting Platform-specific objects =====
- Deleting Organization Profile: SANTANDER_ORG_PROFILE
- Deleting NSG Template: SANTANDER_TEMPLATE
- Deleting VSC Profile: vsc_profile1
- Deleting Infrastructure GW Profile: CATS

```

# Status
Currently VSD objects corresponding to the following structure can be deleted:
```yaml
Organization:
  L2_Domain_Template:
  L3_Domain_Template:
  NSG_instance:
    Port:
    Vlan:
      CE-PE_BGP_Neighbor:
  Routing_Policy:
  BGP_Profile:
  Network_Macro:
  Network_Macro_Group
  L3_Domain:
    Policy_Group:
    Routing_Policy:
    Zone:
      Subnet:
        vPort:
          BridgeInterface:
        VM:
        CE-CPE_BGP_Neighbor:
    ACL Ingress:
      Rule:
    ACL Egress:
      Rule:
  L2_Domain:
    Policy_Group:
    VM:
    ACL Ingress:
      Rule:
    ACL Egress:
      Rule:
Platform:
  nsg_template:
  vsc_profile:
  infrastructure_gw_profile:
```
It is expected that this script will constantly add more _target_ objects.