import argparse
import logging
import time

from bambou.exceptions import BambouHTTPError
from vspk import v4_0 as vspk


def parse_args():
    """
    Parse CLI arguments
    """
    parser = argparse.ArgumentParser(
        description='A script to delete various VSD objects')
    parser.add_argument('-E', '--api_endpoint', required=True,
                        help='Nuage VSD API endpoint')
    parser.add_argument('-P', '--api_endpoint_port', default='8443',
                        help='Nuage VSD API endpoint port (default: 8443)')
    parser.add_argument('-U', '--api_user_name', default='csproot',
                        help='Nuage VSD API username (default: csproot)')
    parser.add_argument('-S', '--api_user_passwd', default='csproot',
                        help='Nuage VSD API username (default: csproot)')
    parser.add_argument('-O', '--api_user_org', default='csp',
                        help='Nuage VSD API username (default: csproot)')
    parser.add_argument('-o', '--org', default=None,
                        help='Name of the Organization to delete')
    parser.add_argument('-n', '--no_globals', default=False, action='store_true',
                        help='Set this flag if you do not want to delete platform settings (Org profiles, NSG Templates, etc)')
    return parser.parse_args()


def job_handler(job):
    """
    Waits for job to succeed and returns job.result
    """

    while True:
        job.fetch()
        if job.status == 'SUCCESS':
            return (True, job.result)
        if job.status == 'FAILED':
            return (False, None)
        time.sleep(1)


def delete_organization(org_name):
    if org_name:
        enterprises = csproot.enterprises.get(
            filter='name is "{}"'.format(org_name))
        if not enterprises:
            print('ERROR: Organization "{}" does not exist in VSD!'.format(org_name))
            exit(-1)
    else:
        enterprises = csproot.enterprises.get()

    for cur_ent in enterprises:
        print('\nDeleting entities inside Organization %s' % cur_ent.name)

        for cur_vm in cur_ent.vms.get():
            print('- Deleting VM "{}"'.format(cur_vm.name))
            cur_vm.delete()

        for cur_domain in cur_ent.domains.get():
            print('- Deleting entities in L3 Domain: %s' % cur_domain.name)

            for subnet in cur_domain.subnets.get():
                print('-- Deleting entities in Subnet: %s' % subnet.name)

                for vport in subnet.vports.get():

                    for br_iface in vport.bridge_interfaces.get():
                        print(
                            '--- Deleting Bridge Interface: {}'.format(br_iface.name))
                        br_iface.delete()

                    print('--- Deleting vPort: {}'.format(vport.name))
                    vport.delete()

                for bgp_neighbor in subnet.bgp_neighbors.get():
                    print('--- Deleting BGP Neighbor {}'.format(bgp_neighbor.name))
                    bgp_neighbor.delete()

                print('-- Deleting Subnet name:%s address:%s netmask:%s' %
                      (subnet.name, subnet.address, subnet.netmask))
                subnet.delete()

            for cur_zone in cur_domain.zones.get():
                print('-- Deleting Zone: %s' % cur_zone.name)
                cur_zone.delete()

            for cur_acl in cur_domain.ingress_acl_templates.get():
                print('-- Deleting entities in Ingress ACL: %s' % cur_acl.name)
                for cur_rule in cur_acl.ingress_acl_entry_templates.get():
                    print('--- Deleting Rule: %s' % cur_rule.description)
                print('-- Deleting Ingress ACL: %s' % cur_acl.name)
                cur_acl.delete()

            for cur_acl in cur_domain.egress_acl_templates.get():
                print('-- Deleting entities in Engress ACL: %s' % cur_acl.name)

                for cur_rule in cur_acl.egress_acl_entry_templates.get():
                    print('--- Deleting Rule: %s' % cur_rule.description)

                print('-- Deleting Egress ACL: %s' % cur_acl.name)
                cur_acl.delete()

            for routing_policy in cur_domain.routing_policies.get():
                if routing_policy.parent_type == 'domain':
                    print('-- Deleting Routing Policy: {}'.format(routing_policy.name))
                    routing_policy.delete()

            for policy_group in cur_domain.policy_groups.get():
                print('-- Deleting Policy Group: %s' % policy_group.name)
                policy_group.delete()

            try:
                cur_domain.delete()
                print('- Deleting L3 Domain: %s' % cur_domain.name)
            except BambouHTTPError as exc:
                raise AssertionError(exc.message)

        for l2_domain in cur_ent.l2_domains.get():
            print('- Deleting entities in L2 Domain: %s' % l2_domain.name)

            for policy_group in l2_domain.policy_groups.get():
                print('-- Deleting Policy Group: %s' % policy_group.name)
                policy_group.delete()

            print('- Deleting L2 Domain: %s' % l2_domain.name)
            l2_domain.delete()

        for cur_l3_domain_template in cur_ent.domain_templates.get():
            print('- Deleting L3 Domain Template: %s' %
                  cur_l3_domain_template.name)
            cur_l3_domain_template.delete()

        for cur_l2_domain_template in cur_ent.l2_domain_templates.get():
            print('- Deleting L2 Domain Template: %s' %
                  cur_l2_domain_template.name)
            cur_l2_domain_template.delete()

        for nsg_instance in cur_ent.ns_gateways.get():

            for port in nsg_instance.ns_ports.get():

                for vlan in port.vlans.get():

                    for cepe_bgp_neighbor in vlan.bgp_neighbors.get():
                        cepe_bgp_neighbor.delete()
                        print('-- BGP Neighbor: {}'.format(cepe_bgp_neighbor.name))

            if nsg_instance.bootstrap_status == 'ACTIVE':
                job = vspk.NUJob(command='CERTIFICATE_NSG_REVOKE')
                nsg_instance.create_child(job)
                if not job_handler(job)[0]:
                    print(
                        '- ERROR: Failed to revoke certificates for NSG Instance: {}'
                        .format(nsg_instance.name))
            try:
                nsg_instance.delete()
                print('- Deleting NSG Instance: {}'.format(nsg_instance.name))
            except BambouHTTPError as exc:
                raise AssertionError(exc.message)

        for bgp_profile in cur_ent.bgp_profiles.get():
            print('- Deleting BGP Profile: {}'.format(bgp_profile.name))
            bgp_profile.delete()

        for routing_policy in cur_ent.routing_policies.get():
            print('- Deleting Routing Policy: {}'.format(routing_policy.name))
            routing_policy.delete()

        for net_macro in cur_ent.enterprise_networks.get():
            print('- Deleting Network Macro: {}'.format(net_macro.name))
            net_macro.delete()

        for net_macro_group in cur_ent.network_macro_groups.get():
            print('- Deleting Network Macro: {}'.format(net_macro_group.name))
            net_macro_group.delete()

        print('- Deleting Organization: %s' % cur_ent.name)
        cur_ent.delete()


def delete_platform_objs():
    print('\n ==== Deleting Platform-specific objects =====')
    for org_profile in csproot.enterprise_profiles.get():
        try:
            org_profile.delete()
            print('- Deleting Organization Profile: {}'.format(org_profile.name))
        except BambouHTTPError:
            # you can not delete default Org profile
            pass

    for nsg_template in csproot.ns_gateway_templates.get():
        print('- Deleting NSG Template: {}'.format(nsg_template.name))
        nsg_template.delete()

    for vsc_profile in csproot.infrastructure_vsc_profiles.get():
        print('- Deleting VSC Profile: {}'.format(vsc_profile.name))
        vsc_profile.delete()

    for infra_gw_profile in csproot.infrastructure_gateway_profiles.get():
        print('- Deleting Infrastructure GW Profile: {}'.format(infra_gw_profile.name))
        infra_gw_profile.delete()


if __name__ == '__main__':

    args = parse_args()

    nuage_session = vspk.NUVSDSession(
        username=args.api_user_name,
        password=args.api_user_passwd,
        enterprise=args.api_user_org,
        api_url='{}:{}'.format(args.api_endpoint, args.api_endpoint_port))

    csproot = nuage_session.start().user

    delete_organization(org_name=args.org)

    if not args.no_globals:
        delete_platform_objs()
